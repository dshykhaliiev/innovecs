import styles from './styles.module.scss';
import { InputBase, Paper } from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import PropTypes from 'prop-types';

const SearchBar = ({ placeholder, onChangeCallback }) => {
    return (
        <Paper className={styles.searchBar}>
            <InputBase
                className={styles.searchBar__input}
                placeholder={placeholder}
                onChange={onChangeCallback}
            />
            <SearchIcon className={styles.searchBar__icon} />
        </Paper>
    );
};

SearchBar.propTypes = {
    placeholder: PropTypes.string,
    onChangeCallback: PropTypes.func,
};

export { SearchBar };
