import { InputBase, Paper } from '@mui/material';
import styles from './styles.module.scss';
import ArrowDropUpIcon from '@mui/icons-material/ArrowDropUp';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import { useState } from 'react';
import PropTypes from 'prop-types';

const CounterBox = ({ defaultCount, onChangeCallback }) => {
    const [count, setCount] = useState(defaultCount);

    const btnClickHandler = (isUp) => {
        const diff = isUp ? 1 : -1;
        const newCount = count + diff;
        setCount(newCount);
        onChangeCallback(newCount);
    };

    return (
        <Paper className={styles.counterBox}>
            <InputBase value={count} className={styles.counterBox__text} />
            <div className={styles.counterBox__upBtn} onClick={() => btnClickHandler(true)}>
                <ArrowDropUpIcon />
            </div>

            <div className={styles.counterBox__downBtn} onClick={() => btnClickHandler(false)}>
                <ArrowDropDownIcon />
            </div>
        </Paper>
    );
};

CounterBox.propTypes = {
    defaultCount: PropTypes.number,
    onChangeCallback: PropTypes.func,
};

export { CounterBox };
