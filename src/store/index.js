import { configureStore } from '@reduxjs/toolkit';
import { rootReducer } from '../reducer';
import storage from 'redux-persist/lib/storage';
import { persistReducer, persistStore } from 'redux-persist';
import thunk from 'redux-thunk';

const persistConfig = {
    key: 'root',
    storage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);
const rootStore = configureStore({
    reducer: persistedReducer,
    middleware: [thunk],
});

const persistor = persistStore(rootStore);

export { rootStore, persistor };
