import { combineReducers } from '@reduxjs/toolkit';
import organizationsReducer from '../containers/Organizations/slices/slice';

const rootReducer = combineReducers({
    organizations: organizationsReducer,
});

export { rootReducer };
