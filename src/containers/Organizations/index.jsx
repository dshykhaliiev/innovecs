import { Paper } from '@mui/material';
import styles from './styles.module.scss';
import { TopPanel } from './components/TopPanel';
import { useDispatch, useSelector } from 'react-redux';
import { useCallback, useMemo, useState } from 'react';
import { addOrganization, selectOrganizations, updateOrganizationData } from './slices/slice';
import { CardsList } from './components/CardsList';

const Organizations = () => {
    const dispatch = useDispatch();
    const [filter, setFilter] = useState('');
    const organizations = useSelector(selectOrganizations);

    const addOrganizationHandler = () => {
        dispatch(addOrganization());
    };

    const cardCounterChangedHandler = (cardId, isTracking, count) => {
        dispatch(updateOrganizationData({ cardId, isTracking, count }));
    };

    const onFilterChanged = useCallback((event) => {
        setFilter(event.target.value);
    }, []);

    const filteredList = useMemo(() => {
        return organizations.filter((item) => {
            const nameLower = item.name.toLowerCase();
            const filterLower = filter.toLowerCase();
            return nameLower.includes(filterLower);
        });
    }, [organizations, filter]);

    return (
        <Paper className={styles.organizations}>
            <div>
                <TopPanel
                    organizationsNum={filteredList.length}
                    onFilterChangedCallback={onFilterChanged}
                    addBtnCallback={addOrganizationHandler}
                />
                <hr className={styles.organizations__separator} />
                <CardsList cards={filteredList} onCardCounterChanged={cardCounterChangedHandler} />
            </div>
        </Paper>
    );
};

export { Organizations };
