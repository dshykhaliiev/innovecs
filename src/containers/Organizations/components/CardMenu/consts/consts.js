import EditIcon from '../../../../../assets/img/edit.png';
import GotoIcon from '../../../../../assets/img/goto.png';
import DeleteIcon from '../../../../../assets/img/delete.png';

export const MENU_ITEMS = [
    {
        id: 0,
        icon: EditIcon,
        text: 'Edit',
    },
    {
        id: 1,
        icon: GotoIcon,
        text: 'Go to Organization',
    },
    {
        id: 2,
        icon: DeleteIcon,
        text: 'Delete Organization',
    },
];
