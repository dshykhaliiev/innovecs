import { Menu, MenuItem } from '@mui/material';
import styles from './styles.module.scss';
import { MENU_ITEMS } from './consts/consts';
import PropTypes from 'prop-types';

const CardMenu = ({ anchorEl, isMenuOpen, handleCloseCallback }) => {
    return (
        <Menu anchorEl={anchorEl} open={isMenuOpen} onClose={handleCloseCallback} placement="bottom-end">
            {MENU_ITEMS.map((item) => {
                return (
                    <MenuItem key={item.id} onClick={handleCloseCallback}>
                        <img className={styles.cardMenu__icon} src={item.icon} alt={''} />
                        {item.text}
                    </MenuItem>
                );
            })}
        </Menu>
    );
};

CardMenu.propTypes = {
    anchorEl: PropTypes.element,
    isMenuOpen: PropTypes.bool,
    handleCloseCallback: PropTypes.func,
};

export { CardMenu };
