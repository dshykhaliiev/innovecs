import { Card } from '../Card';
import styles from './styles.module.scss';
import PropTypes from 'prop-types';
import { CardShape } from '../../shapes/shapes';

const CardsList = ({ cards, onCardCounterChanged }) => {
    return (
        <div className={styles.cardsList}>
            {cards.map((card) => {
                return (
                    <Card
                        key={card.id}
                        cardData={card}
                        onCounterChangedCallback={(isTracking, count) =>
                            onCardCounterChanged(card.id, isTracking, count)
                        }
                    />
                );
            })}
        </div>
    );
};

CardsList.propTypes = {
    cards: PropTypes.arrayOf(CardShape),
    onCardCounterChanged: PropTypes.func,
};

export { CardsList };
