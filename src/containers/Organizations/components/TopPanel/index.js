import styles from './styles.module.scss';
import Button from '@mui/material/Button';
import { SearchBar } from '../../../../components/SearchBar';
import PropTypes from 'prop-types';

const TopPanel = ({ organizationsNum, onFilterChangedCallback, addBtnCallback }) => {
    return (
        <div className={styles.topPanel}>
            <div className={styles.topPanel__leftFlexWrapper}>
                <span>All organizations ({organizationsNum})</span>
                <SearchBar placeholder="Search organization" onChangeCallback={onFilterChangedCallback} />
            </div>
            <Button variant="contained" className={styles.topPanel__addButton} onClick={addBtnCallback}>
                Add New Organization
            </Button>
        </div>
    );
};

TopPanel.propTypes = {
    organizationsNum: PropTypes.number,
    onFilterChangedCallback: PropTypes.func,
    addBtnCallback: PropTypes.func,
};

export { TopPanel };
