import styles from './styles.module.scss';
import { ContentInfo } from '../ContentInfo';
import { CardShape } from '../../shapes/shapes';
import PropTypes from 'prop-types';

const CardContent = ({ cardData, onCounterChanged }) => {
    return (
        <div className={styles.cardContent}>
            <div className={styles.cardContent__licensesLabel}>Licenses</div>
            <div className={styles.cardContent__infosWrapper}>
                <ContentInfo
                    title={'Tracking'}
                    usageNum={1245}
                    assignedNum={cardData.tracking.assigned}
                    valueColor={'red'}
                    onCounterChangedCallback={(count) => onCounterChanged(true, count)}
                />
                <ContentInfo
                    title={'Protection'}
                    usageNum={300}
                    assignedNum={cardData.protection.assigned}
                    valueColor={'green'}
                    onCounterChangedCallback={(count) => onCounterChanged(false, count)}
                />
            </div>
        </div>
    );
};

CardContent.propTypes = {
    cardData: CardShape,
    onCounterChanged: PropTypes.func,
};

export { CardContent };
