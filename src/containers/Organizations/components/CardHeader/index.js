import { useCallback, useState } from 'react';
import styles from './styles.module.scss';
import { IconButton } from '@mui/material';
import { MoreVert } from '@mui/icons-material';
import { CardMenu } from '../CardMenu';
import { ORGANIZATION_ICONS } from '../../consts/consts';
import { CardShape } from '../../shapes/shapes';

const CardHeader = ({ cardData }) => {
    const { name, iconId } = cardData;
    const icon = ORGANIZATION_ICONS[iconId];
    const [anchorEl, setAnchorEl] = useState(null);

    const menuOpen = anchorEl !== null;
    const handleClose = useCallback((event) => {
        setAnchorEl(null);
    }, []);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    return (
        <div className={styles.cardHeader}>
            <img className={styles.cardHeader__icon} src={icon} alt={'header icon'} />
            <span className={styles.cardHeader__label}>{name}</span>
            <IconButton className={styles.cardHeader__menuBtn} onClick={handleClick}>
                <MoreVert />
            </IconButton>

            <CardMenu anchorEl={anchorEl} isMenuOpen={menuOpen} handleCloseCallback={handleClose} />
        </div>
    );
};

CardHeader.propTypes = {
    cardData: CardShape,
};

export { CardHeader };
