import styles from './styles.module.scss';
import { CounterBox } from '../../../../components/CounterBox';
import PropTypes from 'prop-types';

const ContentInfo = ({ title, usageNum, assignedNum, valueColor, onCounterChangedCallback }) => {
    const labelStyle =
        valueColor === 'red' ? styles.contentInfo__label__red : styles.contentInfo__label__green;

    return (
        <div className={styles.contentInfo}>
            <div className={styles.contentInfo__title}>{title}</div>

            <div className={styles.contentInfo__useWrapper}>
                <span className={styles.contentInfo__label}>In use: </span>
                <span className={labelStyle}>{usageNum}</span>
            </div>
            <div className={styles.contentInfo__useWrapper}>
                <span className={styles.contentInfo__label}>Assigned: </span>
                <CounterBox defaultCount={assignedNum} onChangeCallback={onCounterChangedCallback} />
            </div>
        </div>
    );
};

ContentInfo.propTypes = {
    title: PropTypes.string,
    usageNum: PropTypes.number,
    assignedNum: PropTypes.number,
    valueColor: PropTypes.string,
    onCounterChangedCallback: PropTypes.func,
};

export { ContentInfo };
