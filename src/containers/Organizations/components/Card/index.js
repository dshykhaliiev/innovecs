import { Paper } from '@mui/material';
import styles from './styles.module.scss';
import { CardHeader } from '../CardHeader';
import { CardContent } from '../CardContent';
import { CardShape } from '../../shapes/shapes';
import PropTypes from 'prop-types';

const Card = ({ cardData, onCounterChangedCallback }) => {
    return (
        <Paper className={styles.card}>
            <CardHeader cardData={cardData} />
            <hr className={styles.card__separator} />
            <CardContent cardData={cardData} onCounterChanged={onCounterChangedCallback} />
        </Paper>
    );
};

Card.propTypes = {
    cardData: CardShape,
    onCounterChangedCallback: PropTypes.func,
};

export { Card };
