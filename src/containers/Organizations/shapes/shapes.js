import PropTypes from 'prop-types';

export const CardShape = PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    iconId: PropTypes.number,
    protection: PropTypes.shape({
        assigned: PropTypes.number,
    }),
    tracking: PropTypes.shape({
        assigned: PropTypes.number,
    }),
});
