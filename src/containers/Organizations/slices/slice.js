import { createSlice } from '@reduxjs/toolkit';
import { ORGANIZATION_ICONS } from '../consts/consts';

const initialState = {
    organizations: [],
};

const slice = createSlice({
    name: 'organizations',
    initialState,
    reducers: {
        addOrganization(state, action) {
            const newOrg = {
                name: 'Organization Name ' + (state.organizations.length + 1),
                id: state.organizations.length + 1,
                iconId: Math.floor(Math.random() * ORGANIZATION_ICONS.length),
                tracking: {
                    assigned: 1200,
                },
                protection: {
                    assigned: 850,
                },
            };

            state.organizations.push(newOrg);
        },

        updateOrganizationData(state, action) {
            const { cardId, isTracking, count } = action.payload;
            const org = state.organizations[cardId - 1];
            const key = isTracking ? 'tracking' : 'protection';

            org[key].assigned = count;
        },
    },
});

export const selectOrganizations = (state) => {
    return state.organizations.organizations;
};
export const { addOrganization, updateOrganizationData } = slice.actions;
export default slice.reducer;
