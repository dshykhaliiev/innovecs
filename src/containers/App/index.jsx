import { Organizations } from '../Organizations';
import { Container } from '@mui/material';

function App() {
    return (
        <Container>
            <Organizations />
        </Container>
    );
}

export default App;
